const jwt = require('jsonwebtoken');
var config = require('../../config');

module.exports = function (req, res, next) {
    try {
        console.log('inner');
        const token = req.headers.authorization.split(" ")[1];
        var decode = jwt.verify(token, config.JWT_Key);
        req.userData = decode;
        next();
    } catch(error) {
        return res.status(401).json({message: 'Auth Failed! JWT ' + error});
    }
}