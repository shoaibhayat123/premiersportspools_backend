"use strict";

/**
 * Pool
 * @description :: Model for storing Users records
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

var countSchema = new Schema({
    id: {
        type: Number, default: 1000000, unique: true
    }
});

// on every save, add the date
// countSchema.pre('save', function(next) {
//     // get the current date
//     autoIncrement.initialize(mongoose.connection); // This is important. You can remove initialization in different file
//     countSchema.plugin(autoIncrement.plugin, {
//         model: 'countSchema',
//         field: 'id',
//         startAt: 1000000,
//         incrementBy: 1
//     });
// });

var Count = mongoose.model('Count',countSchema);

module.exports = Count;
// var connection = mongoose.createConnection("mongodb://localhost/playscoredb");

// // autoIncrement.initialize(connection);
// autoIncrement.initialize(mongoose.connection); // This is important. You can remove initialization in different file
// countSchema.plugin(autoIncrement.plugin, {
//     model: 'countSchema',
//     field: 'id',
//     startAt: 1000000,
//     incrementBy: 1
// });

// var count = new Count();
// count.save(function (err) {
//
// });