"use strict";

/**
 * Pool
 * @description :: Model for storing Users records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var poolSchema = new Schema({
    id: {
        type: Number, required: true, unique: true
    },
    name: {
        type: "string",
        required: true
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    },
    // poolEvent_id: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'PoolEvent',
    //     required: true
    // },
    // poolType_id: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'PoolType',
    //     required: true
    // },
    // league_id: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'Sport',
    //     required: true
    // },
    poolEvent_id: {
        type: "string",
        // required: true,
    },
    poolType_id: {
        type: "string",
        required: true,
    },
    league_id: {
        type: "string",
        required: true,
    },
    password: {
        type: "string",
        required: true,
        default: "12345678"
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    createdBy:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,

    },
    modifiedDate: {
        type: Date,
        default: Date.now
    },
    winnerFreq: {
        type: Number, required: true
    },
    numSets: {
        type: Number, required: true
    },
    maxEntries: {
        type: Number, required: true
    }
});

// on every save, add the date
poolSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.modifiedDate = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.createdDate)
        this.createdDate = currentDate;

    next();
});

// generating a hash
poolSchema.methods.generateHash = function() {
    return bcrypt.hashSync(this.password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
poolSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

var Pool = mongoose.model('Pool',poolSchema);

module.exports = Pool;