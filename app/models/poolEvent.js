"use strict";

/**
 * Event
 * @description :: Model for storing Event records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EventSchema = new Schema({
    id: {
        type: 'string',
        primaryKey: true,
        required: true,
        default: function () {
            return uuid.v4();
        },
        index: true,
        uuidv4: true
    },
    name: {
        type: "string",
        required: true
    },
    startDate: {
        type: Date,
        default: Date.now
    },
    endDate: {
        type: Date,
        default: Date.now
    },
    days: {
        type: [],
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    }
});

var PoolEvent = mongoose.model('PoolEvent', EventSchema);
module.exports = PoolEvent;
