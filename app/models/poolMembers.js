"use strict";

/**
 * Pool
 * @description :: Model for storing Users records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var poolMemSchema = new Schema({

    pool_id: {
        type: Number, required: true
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    user_id:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    modifiedDate: {
        type: Date,
        default: Date.now
    },
    picks: {
        type: Number,
        default: 0
    },
    shortName: {
        type: "string",
        default: ""
    },
    displayName: {
        type: "string"
    }
});

// on every save, add the date
poolMemSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.modifiedDate = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.createdDate)
        this.createdDate = currentDate;

    next();
});

var PoolMem = mongoose.model('PoolMem',poolMemSchema);

module.exports = PoolMem;