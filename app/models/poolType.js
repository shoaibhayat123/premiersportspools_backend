"use strict";

/**
 * Pool Type
 * @description :: Model for storing PoolType records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PoolTypeSchema = new Schema({
    id: {
        type: 'string',
        primaryKey: true,
        required: true,
        default: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
    },
    name:{
        type:'string',
        required: true,
        unique: true
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    createdBy:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    }
});

var PoolType = mongoose.model('PoolType', PoolTypeSchema);
module.exports = PoolType;
