"use strict";

/**
 * Socre Card
 * @description :: Model for storing PoolType records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ScoreCardSchema = new Schema({
    id: {
        type: 'string',
        primaryKey: true,
        required: true,
        default: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
    },
    quater:{
        type:Number,
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    },
    score: {
        type:"string",
        default: ""
    },
    finished: {
        type: "boolean",
        default: false
    },
    pool_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pool',
        required: true
    },
    scoreArr: [],
    scoreArray: [{ type : mongoose.Schema.Types.ObjectId, ref: 'PoolMem' }],
});

var ScoreCard = mongoose.model('ScoreCard', ScoreCardSchema);
module.exports = ScoreCard;
