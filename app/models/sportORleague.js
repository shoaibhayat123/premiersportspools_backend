"use strict";

/**
 * Sport
 * @description :: Model for storing Sport records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SportSchema = new Schema({
    id: {
        type: 'string',
        primaryKey: true,
        required: true,
        default: function () {
            return uuid.v4();
        },
        index: true,
        uuidv4: true
    },
    name: {
        type: "string",
        required: true
    },
    startDate: {
        type: Date,
        default: Date.now
    },
    endDate: {
        type: Date,
        default: Date.now
    },
    days: {
        type: [],
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    isActive: {
        type: "boolean",
        default: true
    },
    isDelete: {
        type: "boolean",
        default: false
    }
});

var Sport = mongoose.model('Sport', SportSchema);
module.exports = Sport;
