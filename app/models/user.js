"use strict";

/**
 * Users
 * @description :: Model for storing Users records
 */

var uuid = require('node-uuid');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var userSchema = new Schema({
    id: {
        type: 'string',
        primaryKey: true,
        required: true,
        default: function () {
          return uuid.v4();
        },
        index: true,
        uuidv4: true
      },
      isActive: {
        type: "boolean",
        default: false
      },
      isDelete: {
        type: "boolean",
        default: false
      },
      userName: {
        type: "string",
        required: true,
        unique: true
      },
      password: {
        type: "string",
        required: true
      },
      email: {
        type: "string",
        unique: true,
        required: true
      },
      createdDate: {
        type: Date,
        default: Date.now,
      },
      createdBy: {
        type: "string",
        default: 'admin'
      },
      modifiedDate: {
        type: Date,
        default: Date.now,
      },
      she  : {
        type: "string"
      },
      phoneNumber : {
        type: "string"
      },
      isAdmin : {
        type: "boolean",
        default: true
      },
});

// on every save, add the date
userSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.modifiedDate = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.createdDate)
        this.createdDate = currentDate;

    next();
});

// generating a hash
userSchema.methods.generateHash = function() {
    return bcrypt.hashSync(this.password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

var User = mongoose.model('User',userSchema);

module.exports = User;