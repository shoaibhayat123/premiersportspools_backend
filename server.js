// =======================
// get the packages we need ============
// =======================
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var path	= require('path');
const nodemailer = require('nodemailer');
var ObjectId = require('mongoose').Types.ObjectId;
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const ses = new AWS.SES();


var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var hostInfo = config.domainInfo;
var User   = require('./app/models/user'); // get our mongoose model
var Count   = require('./app/models/counter');
var PoolMem   = require('./app/models/poolMembers');
var ScoreCard   = require('./app/models/scoreCard');
var PoolEvent   = require('./app/models/poolEvent'); // get our mongoose model
var PoolType   = require('./app/models/poolType'); // get our mongoose model
var Pool   = require('./app/models/pool'); // get our mongoose model
var Sport   = require('./app/models/sportORleague'); // get our mongoose model
var bcrypt = require('bcrypt-nodejs');
var checkAuth = require('./app/middleware/check-auth');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = config.JWT_Key;

// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable


// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// =======================
// routes ================
// =======================
// basic route

// app.get('/', function(req, res) {
//     res.send('Hello! The API is at http://localhost:' + port + '/api');
// });

app.post('/send', function (req, res) {
    const params = {
        Destination: {
            ToAddresses: [req.body.email]
        },
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data:
                        'This message body contains HTML formatting, like <a class="ulink" href="http://docs.aws.amazon.com/ses/latest/DeveloperGuide" target="_blank">Amazon SES Developer Guide</a>.'
                },
                Text: {
                    Charset: 'UTF-8',
                    Data: 'This is the message body in text format.'
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: 'Test email from code'
            }
        },
        ReturnPath: 'aaron.ruther@gmail.com',
        Source: 'aaron.ruther@gmail.com'
    }

    ses.sendEmail(params, function(err, data) {
        if (err) console.log(err, err.stack);
        else console.log(data);
    });
});

app.get('/count', function (req, res) {

    // if(Count)
    Count.findOne().sort('-_id').then(function(count_) {
        console.log('count_',  count_ );
        if(count_) {
            console.log('count_',  count_.id );
            Pool.findOne({id: count_.id}).then(function (pool) {
                if(pool) {
                    // console.log('count id: ' , _count.id)
                    var count = new Count({id: count_.id + 1});
                    count.save().then(function (_count) {
                        // console.log('count id: ' , _count.id)
                        return res.status(200).json(_count);
                    }).catch(function (err) {
                        console.log('count problem');
                        return res.status(500).json({message: 'Error in Create ' + err})
                    });
                }
                else {
                    console.log('Not Found Count Id in Pool');
                    return res.status(200).json(count_);
                }
            }).catch(function (err) {
                console.log('Error in  Pool' + err);
                return res.status(500).json({message: 'Error in Found ' + err})
            });
        }
        else {
            console.log('in cout');
            // Count.nextCount(function(err, count) {
                // count === 1 -> true
                // console.log('in cout');
            var count = new Count({id: 1000000});
            // if(count) {
                 count.save().then(function (_count) {
                    // console.log('count id: ' , _count.id)
                    return res.status(200).json(_count);
                }).catch(function (err) {
                    console.log('count problem');
                    return res.status(500).json({message: 'Error in Create ' + err})
                });
            // }
            // else {
            //     console.log('in cout 2');
            //     var count = new Count();
            //     count.save().then(function (count_) {
            //         return res.status(200).json(count_);
            //     }).catch(function (err) {
            //         return res.status(500).json({message: 'Error in Create ' + err})
            //     });
            // }
            // });

        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Error in Sort ' + err})
    });

    // var count = new Count();
    // count.save().then(function (count_) {
    //     return res.status(200).json(count_);
    // }).catch(function (err) {
    //     return res.status(500).json({message: 'Error in Create ' + err})
    // });
});

// -------------------------User API's ------------------------

// User Singup
app.post('/signup', function(req, res) {
    User.findOne({$or: [
        {email: req.body.email},
        {userName: req.body.userName}
    ]}).exec().then(function (user) {
        if(user) {
            return res.status(409).json({message: 'User Already Exist ' + user});
        }
        else {
            var password = req.body.password;
            req.body.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            var user = new User(req.body);
            user.save().then(function (newUser) {
                if (newUser) {
                    var email = req.body.email;
                    var url = config.url + 'email-verification/' + email;

                    // create reusable transporter object using the default SMTP transport
                    var transporter = nodemailer.createTransport({
                        host: hostInfo.host,
                        port: hostInfo.hostPort,
                        secure: false, // true for 465, false for other ports
                        auth: {
                            user: hostInfo.hostEmail, // generated ethereal user
                            pass: hostInfo.hostPassword  // generated ethereal password
                        },
                        tls: {rejectUnauthorized: false}
                    });

                    // setup email data with unicode symbols
                    var mailOptions = {
                        from: '"Sports 👻" <' + hostInfo.hostEmail + '>', // sender address
                        to: email, // list of receivers
                        subject: 'Please confirm account', // Subject line
                        html: 'Click the following link to confirm your account:</p><p>' + url + '</p>',
                        text: 'Please confirm your account by clicking the following link: ' + url
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions).then(function (info) {
                        console.log('information', info.messageId)
                    }).catch(function (err) {
                        return res.status(500).json({message: 'Send Mail Error ' + err});
                    });
                    return res.status(200).json(newUser);
                }
            }).catch(function (err) {
                return res.status(500).json({message: 'User Create Error ' + err});
            });
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Reocrd Find Error ' + err});
    });
});

// User Login
app.post('/login', function (req, res) {
    User.findOne({$or: [
        {email: req.body.email},
        {userName: req.body.userName}
    ]}).exec().then(function (user) {
        console.log('login user : ' , user);
        if(user) {
            var password = req.body.password;
            var comparePassword = bcrypt.compareSync(password, user.password);
            console.log(user.isActive + '::' + comparePassword + '::' + user.password);
            if(comparePassword && user.isActive) {
                var token = jwt.sign({email : user.email,
                id: user.id},config.JWT_Key, {expiresIn: '1h'}
                );
                return res.status(200).json({message: 'Auth Succesed!',
                token: token});
            }
            else {
                return res.status(401).json({message: 'Auth Failed!'});
            }
        }
        else {
            return res.status(400).json({message: 'User Not Found OR Auth Failed!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Find User Error ' + err});
    })
});

// User Signup verification
app.get('/email-verification/:email', function (req, res)  {
    var email = req.params.email;
    console.log('email', email);
    User.findOneAndUpdate({ email: email }, { isActive: true }, function(err, user) {
        if (err) throw err;

        if(user) {

            var transporter = nodemailer.createTransport({
                host: hostInfo.host,
                port: hostInfo.hostPort,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: hostInfo.hostEmail, // generated ethereal user
                    pass: hostInfo.hostPassword  // generated ethereal password
                },
                tls: {rejectUnauthorized: false}
            });

            // setup email data with unicode symbols
            var mailOptions = {
                from: '"Sports 👻" <' + hostInfo.hostEmail + '>', // sender address
                to: email, // list of receivers
                subject: 'Confirme Successed', // Subject line
                html: 'Your account have been successed' + user.userName + 'now you can continue',
                text: 'Your account have been successed' + user.userName + 'now you can continue'
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    throw error;
                }
                console.log('Message sent: %s', info.messageId);
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                res.writeHead(301, {"Location": "http://localhost:5555/signin?id=0"});
                return res.end();
            });
        }

    });

});

// HomePage After Login
app.get('/users', checkAuth, function (req, res) {
    User.find().then(function (users) {
        return res.status(200).json({count: users.length});
    }).catch(function (err) {
        return res.status(500).json({message: 'Users Error ' + err})
    })
});

app.get('/user/:any', function (req, res) {
    var emailORname = req.params.any;
    console.log('emailORname', emailORname);
    User.findOne({$or: [
        {email: emailORname},
        {userName: emailORname}
    ]}).then(function (user) {
        return res.status(200).json(user);
    }).catch(function (err) {
        return res.status(500).json({message: 'Not Found ' + err})
    })
});

app.get('/user/id/:any', function (req, res) {
    var _id = req.params.any;
    console.log('user _id', _id);
    User.findOne({_id: _id}).then(function (user) {
        return res.status(200).json(user);
    }).catch(function (err) {
        return res.status(500).json({message: 'Not Found ' + err})
    })
});

// Update User
app.put('/user/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    // var user_ = new User(req.body);
    User.findOne({_id: any}).then(function(user) {
        if(user) {
            console.log('user : ' , user);
            user.userName = req.body.userName;
            var password = req.body.password;
            req.body.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            user.password = req.body.password;
            user.email = req.body.email;
            user.phoneNumber = req.body.phoneNumber;
            user.fullName = req.body.fullName;
            User.findOne({$or: [
                {userName: req.body.userName},
                {email: req.body.email}
            ]}).then(function (finduser) {
                console.log('finduser : ' + user._id + '& : ' + finduser );
                if(finduser) {
                    console.log('finduser : ' + user._id + '& : ' + finduser._id );
                    if(user._id.toString() == finduser._id.toString()) {
                        user.save().then(function(type_){
                            return res.status(200).json(type_);
                        }).catch(function (err) {
                            console.log('error 4  :' , err);
                            return res.status(500).json({message: 'Error in Edit ' + err})
                        });
                    }
                    else {
                        return res.status(200).json({message: 'Alreday Exist with Differnet ID ' + err})
                    }
                }
                else {
                    user.save().then(function(type_){
                        return res.status(200).json(type_);
                    }).catch(function (err) {
                        console.log('error 1  :' , err);
                        return res.status(500).json({message: 'Error in Edit ' + err})
                    });
                }
            }).catch(function (err) {
                console.log('error 2  :' , err);
                return res.status(500).json({message: 'Error in User Name Found ' + err})
            });
        }
    }).catch(function (err) {
        console.log('error 3  :' , err);
        return res.status(400).json({message: 'Not Found ' + err})
    });
});


// ------------------------Pool Event API's----------------------------

// Get Pool Events
app.get('/events', function (req, res) {
    PoolEvent.find().populate('createdBy').populate('createdBy').then(function (events) {
        return res.status(200).json(events);
    }).catch(function (err) {
        return res.status(500).json({message: 'Events Error ' + err})
    });
});

app.get('/event/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolEvent.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function (event) {
        return res.status(200).json(event);
    }).catch(function (err) {
        return res.status(500).json({message: 'Not Found ' + err})
    });
});

app.get('/event/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolEvent.findOne({_id: any}).populate('createdBy').then(function (event) {
        return res.status(200).json(event);
    }).catch(function (err) {
        return res.status(500).json({message: 'Not Found ' + err})
    });
});

// Update Pool Events
app.put('/event/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    var poolEvent = new PoolEvent(req.body);
    PoolEvent.findOneAndUpdate({$or: [
        {_id: any},
        {id: any},
        {name: any}
    ]}, poolEvent).populate('createdBy').then(function(event) {
        console.log(event);
        return res.status(200).json(event);
    }).catch(function (err) {
        return res.status(500).json({message: 'Not Found OR Edit ' + err})
    });
});

// Delete Pool Events
app.get('/event/del/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolEvent.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function (event) {
        event.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

app.get('/event/del/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolEvent.findOne({_id: any}).populate('createdBy').then(function (event) {
        event.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

// Add Pool Events
app.post('/event', function (req, res) {
    User.findOne({_id: req.body.createdBy}).then(function (user) {
        if(user && user.isActive && !user.isDelete) {
            PoolEvent.findOne({name: req.body.name}).then(function (event) {
                if (event) {
                    return res.status(409).json({message: 'Pool Event Already Exist ' + event});
                }
                req.body.startDate = new Date(req.body.startDate);
                req.body.endDate = new Date(req.body.endDate);
                var poolEvent = new PoolEvent(req.body);
                poolEvent.save().then(function (evetn_) {
                    return res.status(200).json(evetn_);
                }).catch(function (err) {
                    return res.status(500).json({message: 'Error in Create ' + err})
                });
            }).catch(function (err) {
                return res.status(400).json({message: 'Pool Event Not Found ' + err})
            });
        }
        else {
            return res.status(400).json({message: 'User Not Found OR Not Active ' + err})
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'User Not Found ' + err})
    });
});


// ------------------------Sport OR League API's----------------------------

// Get Sport
app.get('/sports', function (req, res) {
    Sport.find().populate('createdBy').then(function (sports) {
        return res.status(200).json(sports);
    }).catch(function (err) {
        return res.status(400).json({message: 'Events Error ' + err})
    })
});

app.get('/sport/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Sport.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function (sport) {
        return res.status(200).json(sport);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

app.get('/sport/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Sport.findOne({_id: any}).populate('createdBy').then(function (sport) {
        return res.status(200).json(sport);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

// Update Sport
app.put('/sport/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    var sport_ = new Sport(req.body);
    Sport.findOneAndUpdate({$or: [
        {_id: any},
        {id: any},
        {name: any}
    ]}, sport_).populate('createdBy').then(function(sport) {
        console.log(sport);
        return res.status(200).json(sport);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found OR Edit ' + err})
    });
});

// Delete Sport
app.get('/sport/del/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Sport.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function (sport) {
        sport.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

app.get('/sport/del/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Sport.findOne({_id: any}).populate('createdBy').then(function (sport) {
        sport.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

// Add Sport
app.post('/sport', function (req, res) {
    User.findOne({_id: req.body.createdBy}).then(function (user) {
        if(user && user.isActive && !user.isDelete) {
            Sport.findOne({name: req.body.name}).then(function (league) {
                if (league) {
                    return res.status(409).json({message: 'Sport OR League Already Exist ' + league});
                }
                req.body.startDate = new Date(req.body.startDate);
                req.body.endDate = new Date(req.body.endDate);
                var sport = new Sport(req.body);
                sport.save().then(function (sport_) {
                    return res.status(200).json(sport_);
                }).catch(function (err) {
                    return res.status(500).json({message: 'Error in Create ' + err})
                });
            }).catch(function (err) {
                return res.status(400).json({message: 'Sport OR League Not Found ' + err})
            });
        }
        else {
            return res.status(400).json({message: 'User Not Found OR Not Active ' + err})
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'User Not Found ' + err})
    });
});

// ------------------------Pool Type API's----------------------------

// Get Pool Type
app.get('/types', function (req, res) {
    PoolType.find().populate('createdBy').then(function (types) {
        return res.status(200).json(types);
    }).catch(function (err) {
        return res.status(400).json({message: 'Events Error ' + err})
    })
});

app.get('/type/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolType.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function (type) {
        return res.status(200).json(type);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

app.get('/type/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolType.findOne({_id: any}).populate('createdBy').then(function (type) {
        return res.status(200).json(type);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

// Update Pool Type
app.put('/type/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    var poolType = new PoolType(req.body);
    PoolType.findOne({$or: [
        {_id: any},
        {id: any},
        {name: any}
    ]}).populate('createdBy').then(function(type) {
        if(type) {
            console.log(type);
            type.name = req.body.name;
            type.save().then(function(type_){
                return res.status(200).json(type_);
            }).catch(function (err) {
                return res.status(500).json({message: 'Error in Edit ' + err})
            });
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

// app.put('/type/:any', function (req, res) {
//     var any = req.params.any;
//     console.log('any', any);
//     var poolType = req.body;
//     PoolType.findOneAndUpdate({$or: [
//         {id: any},
//         {name: any}
//     ]}, poolType).then(function(type) {
//         return res.status(200).json(type);
//     }).catch(function (err) {
//         return res.status(400).json({message: 'Not Found ' + err})
//     });
// });

// Delete Pool Type
app.get('/type/del/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolType.findOne({$or: [
        {id: any},
        {name: any}
    ]}).then(function (type) {
        type.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

app.get('/type/del/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolType.findOne({_id: any}).then(function (type) {
        type.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

// Add Pool Type
app.post('/type', function (req, res) {
    User.findOne({_id: req.body.createdBy}).then(function (user) {
        if(user && user.isActive && !user.isDelete) {
            PoolType.findOne({name: req.body.name}).then(function (type) {
                if (type) {
                    return res.status(409).json({message: 'Pool Type Already Exist ' + type});
                }
                var poolType = new PoolType(req.body);
                poolType.save().then(function (type_) {
                    return res.status(200).json(type_);
                }).catch(function (err) {
                    return res.status(500).json({message: 'Error in Create ' + err})
                });
            }).catch(function (err) {
                return res.status(400).json({message: 'Pool Type Not Found ' + err})
            });
        }
        else {
            return res.status(400).json({message: 'User Not Found OR Not Active ' + err})
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'User Not Found ' + err})
    });
});

// ------------------------Pool API's----------------------------

// Get Pool
app.get('/pools', function (req, res) {
    Pool.find().populate('createdBy').populate('poolEvent_id')
        .populate('poolType_id').populate('league_id')
        .populate('members').then(function (pools) {
        return res.status(200).json(pools);
    }).catch(function (err) {
        return res.status(400).json({message: 'Events Error ' + err})
    })
});

// app.get('/pool/:any', function (req, res) {
//     var any = req.params.any;
//     console.log('any', any);
//     Pool.findOne({$or: [
//         {id: any},
//         {name: any}
//     ]}).populate('createdBy').populate('poolEvent_id')
//         .populate('poolType_id').populate('league_id')
//         .populate('members').then(function (pool) {
//         return res.status(200).json(pool);
//     }).catch(function (err) {
//         return res.status(400).json({message: 'Not Found ' + err})
//     })
// });

app.get('/pool/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Pool.findOne({$or: [
        {id: any},
        {name: any}
    ]}).populate('createdBy').populate('members').then(function (pool) {
        return res.status(200).json(pool);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

// app.get('/pool/id/:any', function (req, res) {
//     var any = req.params.any;
//     console.log('any', any);
//     Pool.findOne({_id: any}).populate('createdBy').populate('poolEvent_id')
//         .populate('poolType_id').populate('league_id')
//         .populate('members').then(function (pool) {
//             pool.password = decrypt(pool.password);
//         return res.status(200).json(pool);
//     }).catch(function (err) {
//         return res.status(400).json({message: 'Not Found ' + err})
//     })
// });

app.get('/pool/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Pool.findOne({_id: any}).populate('createdBy').populate('members').then(function (pool) {
        pool.password = decrypt(pool.password);
        return res.status(200).json(pool);
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

// app.get('/pool/userId/last/:any', function (req, res) {
//     var any = req.params.any;
//     console.log('any', any);
//     Pool.findOne({createdBy: any},{},{ sort: { 'createdDate' : -1 } }).populate('createdBy').populate('poolEvent_id')
//         .populate('poolType_id').populate('league_id')
//         .populate('members').then(function (pool) {
//         pool.password = decrypt(pool.password);
//         return res.status(200).json(pool);
//     }).catch(function (err) {
//         return res.status(400).json({message: 'Not Found ' + err})
//     })
// });

app.get('/pool/userId/last/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Pool.findOne({createdBy: any},{},{ sort: { 'createdDate' : -1 } }).populate('createdBy')
        .populate('members').then(function (pool) {
            if(pool) {
                pool.password = decrypt(pool.password);
                return res.status(200).json(pool);
            }
            else {
                return res.status(400).json({message: 'Pool Not Found '});
            }
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    })
});

// Delete Pool Type
app.get('/pool/del/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Pool.findOne({id: any}).then(function (pool) {
        pool.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

app.get('/pool/del/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    Pool.findOne({_id: any}).then(function (pool) {
        pool.remove(function(err) {
            if (err) {
                return res.status(500).json({message: 'Error Remove' + err})
            }
            return res.status(200).json({message: 'Remove Succesfully!'});
        });
    }).catch(function (err) {
        return res.status(400).json({message: 'Not Found ' + err})
    });
});

app.post('/iscompool', function (req, res) {
   var createdBy = req.body.createdBy;
   var id = req.body.id;
   Pool.findOne({$and: [
       {id: id},
       {createdBy: createdBy}
   ]}).then(function (pool) {
       if(pool) {
           return res.status(200).json({status:true});
       } else {
           return res.status(200).json({status:false});
       }
   }).catch(function (err) {
       return res.status(500).json({message: 'Find Pool Error ' + err});
   });
});

// Pool Login
app.post('/plogin', function (req, res) {
    console.log('pool id', req.body);
    User.findOne({_id: req.body.user_id}).then(function (user) {
        if (user) {
            Pool.findOne(
                {id: req.body.id}).exec().then(function (pool) {
                console.log('login pool : ', pool);
                if (pool) {
                    var password = req.body.password;
                    // var comparePassword = bcrypt.compareSync(password, pool.password);
                    // console.log(pool.isActive + '::' + comparePassword + '::' + pool.password);
                    if (password == decrypt(pool.password) && pool.isActive && !pool.isDelete) {
                        PoolMem.findOne({
                            $and: [
                                {pool_id: req.body.id},
                                {user_id: req.body.user_id}
                            ]
                        }).then(function (poolMem) {
                            if (!poolMem) {
                                var pool_mem = new PoolMem({
                                    pool_id: pool.id,
                                    user_id: req.body.user_id
                                });
                                pool_mem.save().then(function (mem) {
                                    if (mem) {
                                        return res.status(200).json({
                                            message: 'Login Succesed!',
                                            pool: pool
                                        });
                                    }
                                }).catch(function (err) {
                                    return res.status(500).json({message: 'Create Pool Mem Error ' + err});
                                });
                            }
                            else {
                                return res.status(200).json({
                                    message: 'Login Succesed!',
                                    pool: pool
                                });
                            }
                        }).catch(function (err) {
                            return res.status(500).json({message: 'Find Pool Mem Error ' + err});
                        });
                    }
                    else {
                        return res.status(401).json({message: 'Login Failed!'});
                    }
                }
                else {
                    return res.status(400).json({message: 'Pool Not Found OR Login Failed!'});
                }
            }).catch(function (err) {
                return res.status(500).json({message: 'Find Pool Error ' + err});
            });
        } else {
            return res.status(401).json({message: 'User Not Found!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Find User in Pool Error ' + err});
    });
});

// Add Pool
app.post('/pool', function (req, res) {
    User.findOne({_id: req.body.createdBy}).then(function (user) {
        if(user && user.isActive && !user.isDelete) {
            // PoolType.findOne({_id: req.body.poolType_id}).then(function (type) {
            //     if(type && type.isActive && !type.isDelete) {
            //         Sport.findOne({_id: req.body.league_id}).then(function (sport) {
            //             if(sport && sport.isActive && !sport.isDelete) {
            //                 PoolEvent.findOne({_id: req.body.poolEvent_id}).then(function (event) {
            //                     if (event && event.isActive && !event.isDelete) {
                                    Pool.findOne({name: req.body.name}).then(function (_pool) {
                                        if (_pool && _pool.createdBy == req.body.createdBy) {
                                            return res.status(409).json({message: 'Pool Already Exist ' + _pool});
                                        }
                                        var password = req.body.password;
                                        // req.body.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
                                        req.body.password =  encrypt(password);

                                        var pool = new Pool(req.body);
                                        // pool.createdBy = pool.league_id = pool.poolType_id
                                        //     = pool.poolEvent_id = ObjectId("542c2b97bac0595474108b48");
                                        pool.save().then(function (pool_) {
                                            var body = { pool_id : pool_.id,
                                                        user_id : pool_.createdBy};
                                            var poolMem = new PoolMem(body);
                                            poolMem.save().then(function (pool_mem) {
                                                var c = 0;
                                                for(var i = 0; i < pool_.winnerFreq; i++) {
                                                    c = i + 1;
                                                    console.log('done', c);
                                                    var s_body = {pool_id: pool_._id,
                                                        quater: c}
                                                    var score = new ScoreCard(s_body);
                                                    score.save().then(function (sc) {
                                                        console.log('done', sc);
                                                    }).catch(function (err) {
                                                        return res.status(500).json({message: 'Error in Create Score Card' + err})
                                                    });
                                                }
                                                if(c == pool_.winnerFreq) {
                                                    return res.status(200).json(pool_);
                                                }
                                            }).catch(function (err) {
                                                return res.status(500).json({message: 'Error in Create Pool Mem' + err})
                                            });
                                        }).catch(function (err) {
                                            return res.status(500).json({message: 'Error in Create ' + err})
                                        });
                                    }).catch(function (err) {
                                        return res.status(400).json({message: 'Not Found ' + err})
                                    });
            //                     }
            //                     else {
            //                         return res.status(400).json({message: 'Pool Event Not Found OR Not Active ' + err})
            //                     }
            //                 }).catch(function (err) {
            //                     return res.status(400).json({message: 'Pool Event Not Found ' + err})
            //                 });
            //             }
            //             else {
            //                 return res.status(400).json({message: 'Sport OR League Not Found OR Not Active ' + err})
            //             }
            //         }).catch(function (err) {
            //             return res.status(400).json({message: 'Sport OR League Not Found ' + err})
            //         });
            //     }
            //     else {
            //         return res.status(400).json({message: 'Pool Type Not Found OR Not Active ' + err})
            //     }
            // }).catch(function (err) {
            //     return res.status(400).json({message: 'Pool Type Not Found ' + err})
            // });
        }
        else {
            return res.status(400).json({message: 'User Not Found OR Not Active ' + err})
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'User Not Found ' + err})
    });
});

// Pool Roaster Get
app.get('/roaster/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
   PoolMem.find({$or: [
       {pool_id: any},
       {picks: any}
        ]}).populate('user_id').then(function (poolMem) {
       return res.status(200).json({PoolMember: poolMem});
   }).catch(function (err) {
       return res.status(400).json({message: 'Pool Members Not Found ' + err})
   });
});

// Pool Roaster Get By User ID
app.get('/roaster/id/:any', function (req, res) {
    var any = req.params.any;
    console.log('any', any);
    PoolMem.find({user_id: any}).populate('user_id').then(function (poolMem) {
        return res.status(200).json({PoolMember: poolMem});
    }).catch(function (err) {
        return res.status(400).json({message: 'Pool Members Not Found ' + err})
    });
});


// Pool Invites
app.post('/invites', function (req, res) {
    var poolId = req.body.poolId;
    var password = req.body.password;
    var emails = req.body.users;
    var url = req.body.route;
    console.log('emails', emails);
    console.log('url', url);
    var transporter = nodemailer.createTransport({
        host: hostInfo.host,
        port: hostInfo.hostPort,
        secure: false, // true for 465, false for other ports
        auth: {
            user: hostInfo.hostEmail, // generated ethereal user
            pass: hostInfo.hostPassword  // generated ethereal password
        },
        tls: {rejectUnauthorized : false}
    });

    // setup email data with unicode symbols
    var mailOptions = {
        from: '"Sports 👻" <' + hostInfo.hostEmail + '>', // sender address
        to: emails, // list of receivers
        subject: 'Confirme Successed', // Subject line
        html: 'Your Pool Id : ' + poolId + ' and password : ' + password + '  '  + url + ' now you can continue',
        text: 'Your Pool Id : ' + poolId + ' and password : ' + password + '  '  + url + ' now you can continue',
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            throw error;
        }
        console.log('Message sent: %s', info.messageId);
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        return res.status(200).json({message: "succes"});
    });
});

// Get Score Card
app.post('/get_score', function (req, res) {
    var pool_id = req.body.pool_id;
    var quater = req.body.quater;
    console.log('pool_id ' + pool_id +' quater ' + quater);
    Pool.findOne({id: pool_id}).then(function (pool) {
        ScoreCard.findOne({$and : [{pool_id: pool._id},
            {quater: quater}]}).then(function(score) {
            if(score) {
                return res.status(200).json(score);
            } else {
                return res.status(400).json({message: 'No Record Found!'});
            }
        }).catch(function (err) {
            return res.status(500).json({message: 'Score Card Error!'});
        });
    }).catch(function (err) {
        return res.status(500).json({message: 'Pool Search Error!'});
    });
});

//Generating Number
app.post('genNum', function (req, res) {
    var pool_id = req.body.pool_id;
    var quater = req.body.quater;
    Pool.findOne({id: pool_id}).then(function (pool) {
        if(pool) {
            console.log('generate pool', pool);
            ScoreCard.findOne({$and : [{pool_id: pool._id},
                {quater: quater}]}).then(function(score) {
                if (score) {
                    console.log('in score', score);
                    if (score.scoreArray.length == 100) {
                        
                    }
                }
            })
        }
    })
});

// Score Card Post
app.post('/score', function (req, res) {
    var pool_id = req.body.pool_id;
    var quater = req.body.quater;
    var index = req.body.index;
    // var s_name = req.body.s_name;
    var user_id = req.body.user_id;
    console.log('pool _id', pool_id);
    console.log('user_id', user_id);
    Pool.findOne({id: pool_id}).then(function (pool) {
        if(pool) {
            console.log('in pool', pool);
            PoolMem.findOne({
                $and: [{pool_id: pool.id},
                    {user_id: user_id}]
            }).then(function (member) {
                if (member) {
                    console.log('in member', member);
                    // return res.status(200).json(member);
                    ScoreCard.findOne({$and : [{pool_id: pool._id},
                        {quater: quater}]}).then(function(score) {
                        if(score) {
                            console.log('in score', score);
                            if (!score.finished) {
                                // var array = new Array(score.scoreArray);
                                console.log('array', score.scoreArray.length);
                                var count = 0;
                                for (var i = 0; i < score.scoreArray.length; i++) {
                                    if (score.scoreArray[i].toString() === member._id.toString()) {
                                        console.log('array value ' + score.scoreArray[i] + 'what' + member._id);
                                        count++;
                                    }
                                }
                                if (count == pool.maxEntries) {
                                    console.log('count 1 ', count + " ;;;; " + pool.maxEntries);
                                    return res.status(400).json({message: 'You have not more picks!'});
                                } else if (count < pool.maxEntries) {
                                    var push = member._id + "," + member.shortName + "," + index;
                                    if(score.scoreArr.indexOf(push) == -1) {
                                        score.scoreArray.push(member._id);
                                        score.scoreArr.push(push);
                                        console.log('in push', score);
                                        score.save().then(function (sc) {
                                            return res.status(200).json(sc);
                                        }).catch(function (err) {
                                            return res.status(500).json({message: 'Score Card Update Member Error!'});
                                        });
                                    } else {
                                        return res.status(200).json({message: 'Pick Exist!'});
                                    }
                                }
                            } else {
                                return res.status(200).json({message: 'Pool Closed!'});
                            }
                        } else {
                            return res.status(400).json({message: 'Score Card Not Found!'});
                        }
                    }).catch(function (err) {
                        return res.status(500).json({message: 'Score Card Error!'});
                    });
                } else {
                    return res.status(400).json({message: 'Pool Member Not Found!'});
                }
            }).catch(function (err) {
                return res.status(500).json({message: 'Pool Member Error!'});
            });
        }
    }).catch(function (err) {
        return res.status(400).json({message: 'Pool Members Not Found ' + err})
    });
});

// function countInArray(array, what) {
//     return array.filter(function(item) { item == what }).length;
// }

function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

function countInArray(array, what) {
    console.log('array value ' + array + 'what' + what + 'array length ' + array.length);
    var count = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i] === what) {
            console.log('array value ' + array[i] + 'what' + what);
            count++;
        }
    }
    return count;
}

// Get Pool and Join Pool by User ID
// app.get('/get_pool/:u_id', function (req, res) {
//     var user_id = req.params.u_id;
//     console.log('user id ', user_id);
//     joinPools = []; createPools = []; counter = 0;
//     PoolMem.find({user_id: user_id}).exec().then(function (mem) {
//         if(mem) {
//             console.log('mem length', mem.length);
//             for(var i = 0; i < mem.length; i++) {
//                 Pool.findOne({id: mem[i].pool_id}).populate('poolEvent_id')
//                     .populate('poolType_id').populate('league_id').then(function (pool) {
//                     if(pool) {
//                         console.log('pool.createdBy', pool.createdBy);
//                         if (pool.createdBy == user_id) {
//                             // console.log('C pool', pool);
//                             createPools.push(pool);
//                         } else if (pool.createdBy != user_id) {
//                             // console.log('J pool', pool);
//                             joinPools.push(pool)
//                         }
//                     }
//                     counter++;
//                     console.log('counter', counter);
//                     if(counter == mem.length) {
//                         return res.status(200).json({'joinPools': joinPools, 'createPools': createPools});
//                     }
//
//                 }).catch(function (err) {
//                     return res.status(500).json({message: 'Pool Found Error!'});
//                 });
//
//             }
//
//         } else {
//             return res.status(400).json({message: 'No Pool of User Not Found!'});
//         }
//     }).catch(function (err) {
//         return res.status(500).json({message: 'Pool Member Found Error!'});
//     });
// });

app.get('/get_pool/:u_id', function (req, res) {
    var user_id = req.params.u_id;
    console.log('user id ', user_id);
    joinPools = []; createPools = []; counter = 0;
    PoolMem.find({user_id: user_id}).exec().then(function (mem) {
        if(mem) {
            console.log('mem length', mem.length);
            for(var i = 0; i < mem.length; i++) {
                Pool.findOne({id: mem[i].pool_id}).then(function (pool) {
                    if(pool) {
                        console.log('pool.createdBy', pool.createdBy);
                        if (pool.createdBy == user_id) {
                            // console.log('C pool', pool);
                            createPools.push(pool);
                        } else if (pool.createdBy != user_id) {
                            // console.log('J pool', pool);
                            joinPools.push(pool)
                        }
                    }
                    counter++;
                    console.log('counter', counter);
                    if(counter == mem.length) {
                        return res.status(200).json({'joinPools': joinPools, 'createPools': createPools});
                    }

                }).catch(function (err) {
                    return res.status(500).json({message: 'Pool Found Error!'});
                });

            }

        } else {
            return res.status(400).json({message: 'No Pool of User Not Found!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Pool Member Found Error!'});
    });
});

// Get Pool Member
app.post('/get_member', function (req, res) {
    console.log('member body', req.body);
    var pool_id = req.body.pool_id;
    var user_id = req.body.user_id;
    PoolMem.findOne({
        $and: [{pool_id: pool_id},
            {user_id: user_id}]
    }).exec().then(function (member) {
        if (member) {
            return res.status(200).json(member);
        } else {
            return res.status(400).json({message: 'Pool Member Not Found!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Pool Member Error!'});
    });
});

// Update Pool Member
app.post('/member', function (req, res) {
    var pool_id = req.body.pool_id;
    var user_id = req.body.user_id;
    var shortName = req.body.shortName;
    PoolMem.findOne({
        $and: [{pool_id: pool_id},
            {user_id: user_id}]
    }).exec().then(function (member) {
        if (member) {
                PoolMem.findOneAndUpdate({ _id: member._id }, { shortName: shortName }
                    , function(err, mem) {
                        if (err) return res.status(500).json({message: 'Pool Member Found Error!'});

                        mem.shortName = shortName;
                        return res.status(200).json(mem);
                    }
                );
        } else {
            return res.status(400).json({message: 'Pool Member Not Found!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Pool Member Error!'});
    });
});

app.post('/member_d', function (req, res) {
    var pool_id = req.body.pool_id;
    var user_id = req.body.user_id;
    var displayName = req.body.displayName;
    PoolMem.findOne({
        $and: [{pool_id: pool_id},
            {user_id: user_id}]
    }).exec().then(function (member) {
        if (member) {
            PoolMem.findOneAndUpdate({ _id: member._id }, { displayName: displayName }
                , function(err, mem) {
                    if (err) return res.status(500).json({message: 'Pool Member Found Error!'});

                    mem.displayName = displayName;
                    return res.status(200).json(mem);
                }
            );
        } else {
            return res.status(400).json({message: 'Pool Member Not Found!'});
        }
    }).catch(function (err) {
        return res.status(500).json({message: 'Pool Member Error!'});
    });
});

// API ROUTES -------------------
// we'll get to these in a second

app.use(express.static(path.join(__dirname, 'prod')));


// app.get('*', function(req, res) {
//     res.render(path.join('./prod/index.html'));
// });

// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);